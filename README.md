# crypto-taxes-cz
czech crypto tax calculator. 

### Rates
EUR/CZK rates are loaded from csv files in `data` directory. Rates are downloaded from CNB.   
Other historical crypto rates are loaded in runtime from coingecko.com if needed.
