package cryptotax

import cryptotax.data.Entry
import cryptotax.data.EntryType
import cryptotax.data.TaxEntry
import cryptotax.data.Transaction
import java.math.BigDecimal
import java.math.BigDecimal.ONE
import java.math.BigDecimal.ZERO
import java.math.RoundingMode.HALF_UP
import java.util.*


class TaxCalculator {
    private var incomeTotalCZK = ZERO
    private var expenseTotalCZK = ZERO

    private val bag = CryptoEntryBag()
    private val taxEntries = mutableListOf<TaxEntry>()

    fun calc(txs: List<Transaction>, startState: List<Entry>) {
        println("Starting calculations")

        startState.forEach { bag.add(it) }

        txs.sortedBy { it.time }
            .forEach { tx ->
                println("TX ${tx.type.name} ${tx.amount}${tx.baseCurrency} @ ${tx.price}${tx.quoteCurrecny}")

                when (tx.type) {
                    EntryType.BUY -> {
                        bag.add(tx.toBaseEntry())
                        if (!tx.quoteCurrecny.isFiat()) {
                            // buy for another crypto = sell that another crypto (making loss/profit and accountable tax entry)
                            manageSell(tx, Side.QUOTE)
                        }
                    }
                    EntryType.SELL -> {
                        manageSell(tx, Side.BASE)
                        if (!tx.quoteCurrecny.isFiat()) {
                            // sell for another crypto = buy that another crypto
                            bag.add(tx.toQuoteEntry())
                        }
                    }
                }
        }

        CSV.writeTaxOutput(taxEntries)
        CSV.writeRemainingHoldings(bag.cryptoBags.flatMap { it.value })
        println("expenses total: $expenseTotalCZK CZK")
        println("income total:   $incomeTotalCZK CZK")
        println("holdings: ${czkValueOfRemaining()} CZK")
    }

    /**
     * Expenses are applied on selling asset (doesnt matter if exchanged for fiat or other crypto)
     */
    private fun manageSell(tx: Transaction, side: Side) {
        val sellCurrency = if (side == Side.BASE) tx.baseCurrency else tx.quoteCurrecny
        val txAmount = if (side == Side.BASE) tx.amount else tx.amount * tx.price
        val txCzkFeeAmount = ExchangeRates.czkPriceAtTime(tx.fee, tx.feeCurrency, tx.time)
        val txCzkPrice = ExchangeRates.czkPriceAtTime(ONE, sellCurrency, tx.time)

        var remaining = txAmount.setScale(8, HALF_UP)
        while (remaining > ZERO) {
            val bagEntry = bag.pop(sellCurrency) // pop entry from bag (FIFO)

            val usedEntryAmount = if (bagEntry.amount > remaining) remaining else bagEntry.amount

            // calculate ratios - trade or entry may not be used at full amount
            val usedEntryRatio = usedEntryAmount / bagEntry.amount
            val usedTxRatio = usedEntryAmount / txAmount

            val usedCzkFeeAmount = usedEntryRatio * bagEntry.czkFeeAmount

            val expense = ((usedEntryAmount * bagEntry.czkPrice) + (usedTxRatio * txCzkFeeAmount) + usedCzkFeeAmount).roundCzk() // expense + buy gee + sell fee
            val income = (usedEntryAmount * txCzkPrice).roundCzk()

            expenseTotalCZK += expense
            incomeTotalCZK += income
            remaining -= usedEntryAmount

            taxEntries.add(
                TaxEntry(
                    time = tx.time,
                    cryptoCurrency = sellCurrency,
                    amount = usedEntryAmount,
                    totalCzkExpense = expense,
                    totalCzkIncome = income
                )
            )

            if (bagEntry.amount > usedEntryAmount) {
                // entry not used entirely - put it back
                bag.addFirst(
                    Entry(
                        time = bagEntry.time,
                        cryptoCurrency = bagEntry.cryptoCurrency,
                        amount = bagEntry.amount - usedEntryAmount,
                        czkPrice = bagEntry.czkPrice,
                        czkFeeAmount = bagEntry.czkFeeAmount - usedCzkFeeAmount,
                        bagEntry.type
                ))
            }
        }
    }

    private fun czkValueOfRemaining(): BigDecimal =
        bag.cryptoBags.keys.sumOf{ k ->
            bag.cryptoBags[k]!!.toList().sumOf {
                (it.amount * it.czkPrice).roundCzk()
            }
        }
}

enum class Side { BASE, QUOTE }

class CryptoEntryBag {
    val cryptoBags: MutableMap<String, LinkedList<Entry>> = mutableMapOf()

    fun add(entry: Entry) = cryptoBags.computeIfAbsent(entry.cryptoCurrency) { LinkedList() }.addLast(entry)
    fun pop(crypto: String): Entry = cryptoBags[crypto]!!.removeFirst()
    fun addFirst(e: Entry) = cryptoBags[e.cryptoCurrency]!!.addFirst(e)
}

fun BigDecimal.roundCzk(): BigDecimal = this.setScale(2, HALF_UP)
fun BigDecimal.roundCrypto(): BigDecimal = this.setScale(8, HALF_UP)
fun String.isFiat() = listOf("CZK", "EUR", "USD").contains(this)
