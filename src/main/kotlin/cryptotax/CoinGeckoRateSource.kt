package cryptotax

import org.http4k.client.OkHttp
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.http4k.core.Status
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.Instant.now
import java.util.*


object CoinGeckoRateSource {

    /* Use https://api.coingecko.com/api/v3/coins/list to find id for crypto symbol*/
    private val CRYPTO_ID_MAP: Map<String, String> = mapOf(
        "XBT" to "bitcoin",
        "XMR" to "monero",
        "ETH" to "ethereum",
        "ETC" to "ethereum-classic",
        "DASH" to "dash",
        "ADA" to "cardano",
        "OMG" to "omisego",
        "LTC" to "litecoin"
    )
    private val COINGECKO_DATE_FORMAT = SimpleDateFormat("dd-MM-yyyy")
    private val CACHE: MutableMap<String, BigDecimal> = mutableMapOf()
    private val SLEEP_BETWEEEN_REQUESTS: Long = 1100

    private val http by lazy { OkHttp() }
    private var lastCall: Instant = now()

    fun getCzkRate(crypto: String, date: Instant): BigDecimal {
        val id = CRYPTO_ID_MAP[crypto.toUpperCase()] ?: throw NotImplementedError("Please add mapping for $crypto")
        val dateStr = COINGECKO_DATE_FORMAT.format(Date.from(date))
        return CACHE.computeIfAbsent("$id$dateStr") { getHistoryData(id, dateStr) }
    }

    private fun getHistoryData(id: String, date: String): BigDecimal {
        println("Fetching czk rate for $id at $date")

        val durationFromLastCall = now().toEpochMilli() - lastCall.toEpochMilli()
        if (durationFromLastCall < SLEEP_BETWEEEN_REQUESTS) {
            Thread.sleep(SLEEP_BETWEEEN_REQUESTS - durationFromLastCall)
        }

        val response = http(Request(GET,"https://api.coingecko.com/api/v3/coins/$id/history?date=$date")).let {
            if (it.status != Status.OK) {
                println("Error code ${it.status}: ${it.bodyString()}")
            }
            moshi.adapter(CoinGeckoHistoryResponse::class.java).fromJson(it.bodyString())
        }
        lastCall = now()
        return response!!.market_data.current_price["czk"]!!
    }
}

data class CoinGeckoHistoryResponse(
    val id: String,
    val symbol: String,
    val name: String,
    val market_data: MarketData
) {
    data class MarketData(
        val current_price: Map<String, BigDecimal>,
        val market_cap: Map<String, BigDecimal>,
        val total_volume: Map<String, BigDecimal>
    )
}
