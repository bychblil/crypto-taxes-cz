package cryptotax

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*

object ExchangeRates {

    private val EUR_CZK_RATES: TreeMap<Date, BigDecimal> = TreeMap()
    private val CNB_FIAT_DATE_FORMAT = SimpleDateFormat("dd.MM.yyyy")

    init {
        loadEURCZKRates()
    }

    fun czkPriceAtTime(amount: BigDecimal, currency: String, time: Instant): BigDecimal =
        when (currency) {
            "CZK" -> amount
            "EUR" -> getEurCzkRate(Date.from(time)) * amount
            else -> CoinGeckoRateSource.getCzkRate(currency, time) * amount
        }


    private fun getEurCzkRate(key: Date): BigDecimal {
        var e = EUR_CZK_RATES.floorEntry(key)
        if (e == null) {
            e = EUR_CZK_RATES.firstEntry()
        }
        return e.value;
    }

    private fun loadEURCZKRates() {
        sequenceOf(
            "eurczk2017.csv",
            "eurczk2018.csv",
            "eurczk2019.csv",
            "eurczk2020.csv"
        ).forEach { file ->
            csvReader().open("data/$file") {
                readAllAsSequence().forEach { row ->
                    EUR_CZK_RATES[CNB_FIAT_DATE_FORMAT.parse(row[0])] = BigDecimal(row[1])
                }
            }
        }
    }

    @JvmStatic
    fun main(args: Array<String>) {
        ExchangeRates
    }
}