package cryptotax.data

import java.math.BigDecimal
import java.time.Instant

data class TaxEntry(
    val time: Instant,
    val cryptoCurrency: String,
    var amount: BigDecimal,
    val totalCzkExpense: BigDecimal,
    val totalCzkIncome: BigDecimal
)
