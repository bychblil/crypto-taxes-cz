package cryptotax.data

import cryptotax.ExchangeRates
import java.math.BigDecimal
import java.math.BigDecimal.ONE
import java.time.Instant

data class Transaction(
    val time: Instant,
    val baseCurrency: String,
    val quoteCurrecny: String,
    var amount: BigDecimal,
    val price: BigDecimal,
    val fee: BigDecimal,
    val feeCurrency: String,
    val type: EntryType
) {

    fun toBaseEntry() = Entry(
        time = time,
        cryptoCurrency = baseCurrency,
        amount = amount,
        czkPrice = ExchangeRates.czkPriceAtTime(price, quoteCurrecny, time),
        czkFeeAmount = ExchangeRates.czkPriceAtTime(fee, feeCurrency, time),
        type = type
    )

    fun toQuoteEntry() = Entry(
        time = time,
        cryptoCurrency = quoteCurrecny,
        amount = amount * price,
        czkPrice = ExchangeRates.czkPriceAtTime(ONE, quoteCurrecny, time),
        czkFeeAmount = ExchangeRates.czkPriceAtTime(fee, feeCurrency, time),
        type = type
    )
}
