package cryptotax.data

import java.math.BigDecimal
import java.time.Instant

data class Entry(
    val time: Instant,
    val cryptoCurrency: String,
    var amount: BigDecimal,
    val czkPrice: BigDecimal,
    val czkFeeAmount: BigDecimal,
    val type: EntryType
)

enum class EntryType { BUY, SELL }
