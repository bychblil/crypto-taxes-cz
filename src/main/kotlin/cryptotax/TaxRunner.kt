package cryptotax

object TaxRunner {

    // this is file that was exported last year as remaining of holding assets
    private const val REMAININGS_FILE = "data/example-remaining.csv"

    private const val DATA_FILE = "data/example-data.csv"

    @JvmStatic
    fun main(args: Array<String>) {
        TaxCalculator().calc(
            CSV.readTxInput(DATA_FILE),
            CSV.readRemainingEntryInput(REMAININGS_FILE)
        )
    }
}
