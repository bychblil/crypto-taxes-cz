package cryptotax

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import com.github.doyaaaaaken.kotlincsv.dsl.csvWriter
import cryptotax.data.Entry
import cryptotax.data.EntryType
import cryptotax.data.TaxEntry
import cryptotax.data.Transaction
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.time.Instant

object CSV {
    private const val OUTPUT_TAX_FILE = "data/result.csv"
    private const val OUTPUT_HOLDING_FILE = "data/remainings.csv"
    private val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    fun writeTaxOutput(records: List<TaxEntry>) {
        csvWriter().open(OUTPUT_TAX_FILE) {
            writeRow(listOf("time", "cryptoCurrency", "amount", "czk_expense", "czk_income"))
            records.forEach { r ->
                writeRow(
                    listOf(
                        r.time,
                        r.cryptoCurrency,
                        r.amount,
                        r.totalCzkExpense,
                        r.totalCzkIncome
                    )
                )
            }
        }
    }

    fun writeRemainingHoldings(records: List<Entry>) {
        csvWriter().open(OUTPUT_HOLDING_FILE) {
            writeRow(listOf("time", "side", "cryptoCurrency", "amount", "czkPrice", "czkFee"))
            records.forEach { r ->
                writeRow(
                    listOf(
                        r.time,
                        r.type,
                        r.cryptoCurrency,
                        r.amount,
                        r.czkPrice,
                        r.czkFeeAmount
                    )
                )
            }
        }
    }

    fun readTxInput(file: String): List<Transaction> =
        csvReader().open(file) {
            return@open readAllAsSequence()
                .filterIndexed { i, _ -> i != 0 }
                .map { row ->
                    val productStr = row[0].also { assert(it.length == 8) }
                    Transaction(
                        time = DATE_FORMAT.parse(row[1]).toInstant(),
                        baseCurrency = productStr.getBase(),
                        quoteCurrecny = productStr.getQuote(),
                        amount = BigDecimal(row[6]),
                        price = BigDecimal(row[3]),
                        fee = BigDecimal(row[5]),
                        feeCurrency = productStr.getQuote(),
                        type = EntryType.valueOf(row[2].toUpperCase())
                    )
                }.toList()
        }

    fun readRemainingEntryInput(file: String): List<Entry> =
        csvReader().open(file) {
            return@open readAllAsSequence()
                .filterIndexed { i, _ -> i != 0 }
                .map { row ->
                    Entry(
                        time = Instant.parse(row[0]),
                        cryptoCurrency = row[2],
                        amount = BigDecimal(row[3]),
                        czkPrice = BigDecimal(row[4]),
                        czkFeeAmount = BigDecimal(row[5]),
                        type = EntryType.valueOf(row[1].toUpperCase())
                    )
                }.toList()
        }

    fun String.getBase(): String {
        if (this.length ==  6) return this.substring(0,3)
        if (this.length ==  8) return this.substring(1,4)
        if (this.startsWith("DASH")) return this.substring(0,4)
        throw IllegalArgumentException("Cannot parse product $this")
    }

    fun String.getQuote(): String {
        if (this.length ==  6) return this.substring(3)
        if (this.length ==  8) return this.substring(5)
        if (this.startsWith("DASH")) return this.substring(4)
        throw IllegalArgumentException("Cannot parse product $this")
    }
}
